# Copyright (c) 2019 Joseph Nahmias <joe@nahmias.net>
# Distributed under the terms of the Modified BSD License.
# To use, add this to your local magic folder [eg. ~/.ipython/metakernel/magics/]


from math import copysign, fabs
from xml.dom import minidom
from metakernel import Magic
from IPython.display import HTML, display_svg

def _convert_text_to_foreignObject(t):
    text = t.firstChild.nodeValue
    fo = t.ownerDocument.createElement('foreignObject')
    t.parentNode.replaceChild(fo, t)
    old_x = float(t.attributes['x'].value)
    old_y = float(t.attributes['y'].value)
    fontsize = float(t.attributes['font-size'].value)
    new_x = old_x - fontsize*(len(text)-2)/2
    new_y = copysign( fabs(old_y) + fontsize, old_y )
    fo.setAttribute('x', str(new_x))
    fo.setAttribute('y', str(new_y))
    fo.setAttribute('height', str(2*fontsize))
    fo.setAttribute('width', str(fontsize * (len(text)-2)))
    d = t.ownerDocument.createElement('div')
    fo.appendChild(d)
    style_attrs = ['font-family', 'font-size']
    styles = [":".join([k,v]) for k,v in t.attributes.items() if k in style_attrs]
    styles.append('text-align:center')
    d.setAttribute('style', "; ".join(styles))
    d.appendChild(t.ownerDocument.createTextNode(text))

def _elemHasLaTex(e):
    return ( e.firstChild.nodeValue.startswith('$')
         and e.firstChild.nodeValue.endswith('$') )

def _createSVG(dot):
    """ create an SVG from a dot string using pydot """
    try:
        import pydot
    except:
        raise Exception(
            "You need to install pydot.\n"
            "On Debian-based systems install the package python3-pydot."
        )
    (graph,) = pydot.graph_from_dot_data(str(dot))
    svg = graph.create_svg()
    if hasattr(svg, "decode"):
        svg = svg.decode("utf-8")
    s = minidom.parseString(svg)
    for t in s.getElementsByTagName('text'):
        t.normalize()
        if _elemHasLaTex(t): _convert_text_to_foreignObject(t)
    return s.toxml()

class DotMJMagic(Magic):

    def line_dotMJ(self, code):
        """
        %dotMJ CODE - render code as Graphviz image

        This line magic will render the Graphiz CODE, and render 
        it as an image.

        Example:
            %dotMJ graph A { a->b };

        """
        svg = _createSVG(code)
        #display_svg(svg)
        # Note: need to use HTML [instead of SVG] so that MathJax renders
        self.kernel.Display(HTML(svg))

    def cell_dotMJ(self):
        """
        %%dotMJ - render contents of cell as Graphviz image

        This cell magic will send the cell to the browser as
        HTML.

        Example:
            %%dotMJ

            graph A { a->b };
        """
        self.line_dotMJ(self.code)
        self.evaluate = False

def register_magics(kernel):
    kernel.register_magics(DotMJMagic)

def register_ipython_magics():
    from metakernel import IPythonKernel
    from IPython.core.magic import register_cell_magic
    kernel = IPythonKernel()
    magic = DotMJMagic(kernel)

    @register_cell_magic
    def dotMJ(line, cell):
        """
        %%dotMJ - evaluate cell contents as a dot diagram.
        """
        magic.code = cell
        magic.cell_dotMJ()
